# Programming Syntax Guide
A General Purpose Syntax Guide For Python, Java, C#, and Javascript

![Title](Screenshots/Title.png)

## Project Summary 

This is guide provides a semi comprehsive look at a large selection of programming concepts across multiple languages.
Currently this guide was written with <b> Python, Java, C#, and Javascript <b> as supported languages. More languages may be added in the future.

## How To Use
Follow the instructions below to begin using this guide.
1. Download the .pdf or .docx file to your computer
2. Open the file using an appropiate pdf viewer or office suite.
3. Navigate to a desired section by clicking on the link present on the Table Of Contents
4. Scroll down until you have found your chosen language.


## Sections

This Syntax Guide is split into 3 sections.

The 1st section tackes the basic building blocks of any program. 
These include: <b> data types, if statements, variables, comments, and more. <b>

The 2nd section involves more advance concepts, such as <b> arrays, arraylists, functions and methods, etc. <b>

The 3rd and final section focuses on more specilized concepts, these include: <b> substrings, dictonaries, and classes. <b>

### Sub-Sections

Each Section contains multiple sub-sections, these are denoted with the section name followed by a number, such as <b> 1.4 or 3.2

Within each sub-section, each programming language has its own place. Each language contains a general form for the concept touched upon, as well as examples if need by.

There may also be notes for each sub-section, these are indicated with an * by the code that needs clarification.

## Screenshots 
![Title](Screenshots/Ex1.png)

<br>

![Title](Screenshots/Ex2.png)

##
Programming Syntax Guide © 2023 by Jack Levin is licensed under CC BY-SA 4.0 
